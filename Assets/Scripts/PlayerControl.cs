﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{


    [SerializeField] private float speed = 6;
    [SerializeField] private float force = 4;
    [SerializeField] private float minHeight = -20;
    [SerializeField] private Arrow arrow;
    [SerializeField] private Transform arrowSpawnPoint;
    [SerializeField] private float shootForce;
    [SerializeField] private float fireRate ;
    [SerializeField] private int arrowsCount = 3;
    [SerializeField] private Item item;
    [SerializeField] private BuffReciever buffReciever;
    [SerializeField] private Health health;
    public Health Health { get { return health; } }
    private float nextFire;
    private Arrow currentArrow;

    private bool coolDownReady;

    private List<Arrow> arrowPool;

    public Rigidbody2D rigidbody;
    public GroundDetection groundDetection;
    private Vector3 direction;
    public Animator animator;
    public SpriteRenderer spriteRenderer;
    private bool isJumping;

    private float bonusForce;
    private float bonusDamage;
    private float bonusHealth;

    private void Start()
    {
        arrowPool = new List<Arrow>();
        for (int i = 0; i < arrowsCount; i++)
        {
            var arrowTemp = Instantiate (arrow, arrowSpawnPoint);
            arrowPool.Add(arrowTemp);
            arrowTemp.gameObject.SetActive(false);
        }

        buffReciever.OnBuffsChanged += ApplyBuffs;
    }

    private void ApplyBuffs()
    {
        var forceBuff = buffReciever.Buffs.Find(t => t.type == BuffType.Force);
        var damageBuff = buffReciever.Buffs.Find(t => t.type == BuffType.Damage);
        var armorBuff = buffReciever.Buffs.Find(t => t.type == BuffType.Armor);
        bonusForce = forceBuff == null ? 0 : forceBuff.additiveBonus;
        bonusHealth = armorBuff == null ? 0 : armorBuff.additiveBonus;
        health.SetHealth((int)bonusHealth);
        bonusDamage = damageBuff == null ? 0 : damageBuff.additiveBonus;
    }
        


    void FixedUpdate()
    {
        animator.SetBool("isGrounded", groundDetection.isGrounded);
        if (!isJumping && !groundDetection.isGrounded)
            animator.SetTrigger("StartFall");
        isJumping = isJumping && !groundDetection.isGrounded;

        direction = Vector3.zero;

        if (Input.GetKey(KeyCode.A))
            direction = Vector3.left;

        if (Input.GetKey(KeyCode.D))
            direction = Vector3.right;

        direction *= speed;

        direction.y = rigidbody.velocity.y;
        rigidbody.velocity = direction;


        if (Input.GetKeyDown(KeyCode.Space) && groundDetection.isGrounded)
        {
            rigidbody.AddForce(Vector2.up * (force + bonusForce), ForceMode2D.Impulse);
            animator.SetTrigger("StartJump");
            isJumping = true;
        }

        if (transform.position.y < minHeight)
        {
            transform.position = new Vector3(-44,1,0);
        }

        if (direction.x > 0)
            spriteRenderer.flipX = false;
        if (direction.x < 0)
            spriteRenderer.flipX = true;

        animator.SetFloat("Speed", Mathf.Abs(direction.x));    
    }

    private void Update()
    {
        CheckShoot();
    }


    void CheckShoot()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (direction.x == 0)
            {
                if (Time.time > nextFire)
                {
                    animator.SetTrigger("StartShoot");

                    StartCoroutine(FiveSeconds());

                    nextFire = Time.time + fireRate;

                    currentArrow = GetArrowFromPool();

                    currentArrow.SetImpulse
                        (Vector2.right, spriteRenderer.flipX ? -force * shootForce : force * shootForce,(int)bonusDamage, this);

                    animator.SetTrigger("EndShoot");
                }
            }
           
        }

    }

    private IEnumerator FiveSeconds()
    {
        yield return new WaitForSeconds(4);       
        yield break;
    }

    public void ReturnArrowToPool(Arrow arrowTemp)
    {
        if (!arrowPool.Contains(arrowTemp))
            arrowPool.Add(arrowTemp);

        arrowTemp.transform.parent = arrowSpawnPoint;
        arrowTemp.transform.position = arrowSpawnPoint.transform.position;
        arrowTemp.gameObject.SetActive(false);
    }

    private Arrow GetArrowFromPool()
    {
        if (arrowPool.Count > 0)
        {
            var arrowTemp = arrowPool[0];
            arrowPool.Remove(arrowTemp);
            arrowTemp.gameObject.SetActive(true);
            arrowTemp.transform.parent = null;
            arrowTemp.transform.position = arrowSpawnPoint.transform.position;
            return arrowTemp;
        }
        return Instantiate
            (arrow, arrowSpawnPoint.position, Quaternion.identity);
    }
}


