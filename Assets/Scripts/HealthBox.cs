﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBox : MonoBehaviour
{
    public int bonusHealth;
    [SerializeField] private Animator animator;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player") || col.gameObject.CompareTag("Enemy"))
        {
            Health health = col.gameObject.GetComponent<Health>();
            health.SetHealth(bonusHealth);
            animator.SetTrigger("StartDestroy");
        }
    }

    public void EndDestroy()
    {
        Destroy(gameObject);
    }


}
